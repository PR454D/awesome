#+TITLE: README
#+AUTHOR: Prasad Sawool
#+DATE: 09-08-2021
#+PROPERTY: header-args :tangle rc.lua

* My Awesome window manager workflow :toc:
  - [[#gallery][Gallery]]
  - [[#dependencies][Dependencies]]
  - [[#installation][Installation]]
  - [[#basic-keybindings][Basic Keybindings]]
  - [[#highlights-for-configuration][Highlights for configuration]]

** Gallery

[[./awesomewm.png][Awesome wm]]

[[./awesome-rice-nord.png][Awesome nord]]

[[./garuda-awesome.png][Garuda awesome]]

** Dependencies

- rofi - a simple and very customisable launcher
- picom-jonaburg-git(AUR) - a compositor for transparency and preventing screen tearing
  In case you are not on Arch-based distro, you may use [[https://github.com/jonaburg/picom][Picom-jonaburg]] installation instructions
- feh - a simple image viewer and wallpaper setter utility
- lxsession - polkit authentication agent. basically a gui for authentication of password
*** Optional
- transset-df(AUR) - if you are a fan of making all windows transparent.
- nwg-launchers - again a gui application launcher
- emacs - not forcing, but I have keybinds for that in my config, removing it  wont break any other stuff.
** Installation

Install awesome window manager first.Duh!

  #+begin_src bash
git clone --recurse-submodules https://gitlab.com/PR454D/awesome-setup.git ~/.config/awesome
  #+end_src
Logout and log in to Awesome wm session. Enjoy!

** Basic Keybindings

Mod = <Super> or <Meta> or Windows key
|--------+---------+----------------------|
| Action | Keybind | Description          |
|--------+---------+----------------------|
| rofi   | Mod + z | Application launcher |
|--------+---------+----------------------|
| Help   | Mod + s | show all keybinds    |
|--------+---------+----------------------|
|        |         |                      |

** Highlights for configuration
