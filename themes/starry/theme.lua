theme = {}
local os = os
theme.font = "Fantasque Sans Mono 13"

theme.bg_normal = "#24283b"
theme.bg_focus = "#24283b"
theme.bg_urgent = "#1f2335"
theme.bg_minimize = "#1D202F"
theme.bg_systray = theme.bg_normal

theme.fg_normal = "#c0caf5"
theme.fg_focus = "#7aa2f7"
theme.fg_urgent = "#7aa2f7"
theme.fg_minimize = "#1D202F"

theme.border_width = 1
theme.border_normal = "#292e42"
theme.border_focus = "#7aa2f7"
theme.border_marked = "#f7768e"

-- There are other variable sets
-- overriding the blackarch one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
theme.taglist_bg_focus = "#1D202F"
theme.titlebar_bg_focus = "#1D202F"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = "/usr/share/awesome/themes/blackarch/submenu.png"
theme.menu_height = 12
theme.menu_width = 100
theme.menu_border_color = "#1D202F"

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
theme.bg_widget = "#7dcfff"

-- Define the image to load
theme.titlebar_close_button_normal = "/usr/share/awesome/themes/blackarch/titlebar/close_normal.png"
theme.titlebar_close_button_focus = "/usr/share/awesome/themes/blackarch/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = "/usr/share/awesome/themes/blackarch/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive = "/usr/share/awesome/themes/blackarch/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "/usr/share/awesome/themes/blackarch/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active = "/usr/share/awesome/themes/blackarch/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "/usr/share/awesome/themes/blackarch/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive = "/usr/share/awesome/themes/blackarch/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "/usr/share/awesome/themes/blackarch/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active = "/usr/share/awesome/themes/blackarch/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive =
	"/usr/share/awesome/themes/blackarch/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive =
	"/usr/share/awesome/themes/blackarch/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "/usr/share/awesome/themes/blackarch/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active = "/usr/share/awesome/themes/blackarch/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive =
	"/usr/share/awesome/themes/blackarch/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive =
	"/usr/share/awesome/themes/blackarch/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active =
	"/usr/share/awesome/themes/blackarch/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active = "/usr/share/awesome/themes/blackarch/titlebar/maximized_focus_active.png"

theme.wallpaper = os.getenv("HOME") .. "/.config/awesome/themes/starry/awesome-wp.png"
--theme.wallpaper = "/opt/wallpapers/Space.jpg"
--theme.wallpaper = "~/Downloads/Live Wallpapers/City_nightlight.mp4"

-- You can use your own layout icons like this:
theme.layout_fairh = "~/.config/awesome/themes/starry/fairh.png"
theme.layout_cornernw = "~/.config/awesome/themes/starry/cornernw.png"
theme.layout_fairv = "~/.config/awesome/themes/starry/fairv.png"
theme.layout_floating = "~/.config/awesome/themes/starry/floating.png"
theme.layout_magnifier = "~/.config/awesome/themes/starry/magnifier.png"
theme.layout_max = "~/.config/awesome/themes/starry/max.png"
theme.layout_fullscreen = "~/.config/awesome/themes/starry/fullscreen.png"
theme.layout_tilebottom = "~/.config/awesome/themes/starry/tilebottom.png"
theme.layout_tileleft = "~/.config/awesome/themes/starry/tileleft.png"
theme.layout_tile = "~/.config/awesome/themes/starry/tile.png"
theme.layout_tiletop = "~/.config/awesome/themes/starry/tiletop.png"
theme.layout_spiral = "~/.config/awesome/themes/starry/spiral.png"
theme.layout_dwindle = "~/.config/awesome/themes/starry/dwindle.png"

theme.awesome_icon = "/usr/share/awesome/themes/blackarch/blackarch.png"

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
